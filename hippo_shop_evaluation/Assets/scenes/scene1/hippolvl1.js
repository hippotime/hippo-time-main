﻿#pragma strict

var h1 : AudioClip;
var h2 : AudioClip;
var h3 : AudioClip;
var h4 : AudioClip;
var h5 : AudioClip;
var song : AudioClip;

internal var animator : Animator;
internal var c = new Common();
internal var isBreathing = false;
internal var isWalking = true;
internal var isWaving = false;

internal static var moveSpeed = -2.45f;

function Start () {
	animator = gameObject.GetComponent.<Animator>();
}

function Update () {
	if (c.GetTime() < 0) {
		transform.position = c.MoveToLocation(-15f, -1.39f, 0f, 0f, -8f, 0f, c.GetTime());
	}

	if (c.TimeIs (-2.7)) {
		isWalking = false;
	}

	if (c.TimeIs (0.0)) {
		GetComponent.<AudioSource>().PlayOneShot(h1);
	}

	if (c.TimeIs (10.0)) {
		GetComponent.<AudioSource>().PlayOneShot(h2);
	}

	if (c.TimeIs (32.0)) {
		isBreathing = true;
		GetComponent.<AudioSource>().PlayOneShot(song);
	}

	if (c.TimeIs (44.0)) {
		GetComponent.<AudioSource>().PlayOneShot(song);
	}

	if (c.TimeIs (56.0)) {
		GetComponent.<AudioSource>().PlayOneShot(song);
	}

	if (c.TimeIs (62.0)) {
		isBreathing = false;
	}

	if (c.TimeIs (70.0)) {
		GetComponent.<AudioSource>().PlayOneShot(h3);
	}

	if (c.TimeIs (82.0)) {
		GetComponent.<AudioSource>().PlayOneShot(h4);
	}

	if (c.TimeIs (84.0)) {
		isWaving = true;
	}

	if (c.TimeIs (86.0)) {
		GetComponent.<AudioSource>().PlayOneShot(h5);
		isWaving = false;
	}

	if (c.TimeIs (88.0)) {
		isWalking = true;
	}

	if (c.GetTime() > 91) {
		transform.position = c.MoveToLocation(-2f, 15f, 0f, 0f, 91f, 96f, c.GetTime());
	}

	animator.SetBool("isBreathing", isBreathing);
	animator.SetBool("isWalking", isWalking);
	animator.SetBool("isWaving", isWaving);

}
