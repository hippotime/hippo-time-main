﻿#pragma strict


internal var animator : Animator;
internal var c = new Common();

var e1: AudioClip;
var e2: AudioClip;

internal var worried = true;

function Start () {
	animator = gameObject.GetComponent.<Animator>();
}

function Update () {
	if(c.TimeIs (39.0)){
		worried = false;
	}
	if(c.TimeIs (41.0)){
		GetComponent.<AudioSource>().PlayOneShot(e1);
	}

	if(c.TimeIs(71.0)){
		GetComponent.<AudioSource>().PlayOneShot(e2);
	}

	animator.SetBool("worried", worried);
}
