﻿#pragma strict

internal var animator : Animator;
private var obj: GameObject;
internal var c = new Common();


internal var waving = false;
internal var tantrum = true;
internal var tantrumEnd = false;
internal var angryEnd = false;


var l1: AudioClip;
var l2: AudioClip;
var l3: AudioClip;

function Start () {
	animator = gameObject.GetComponent.<Animator>();
	obj = GameObject.FindWithTag("lion");
}

function Update () {
	//Start at -7.2
	//make it disappear
	//obj.SetActiveRecursively(false);
	if(c.TimeIs (-5.0)){
		GetComponent.<AudioSource>().PlayOneShot(l1);
		tantrum = false;
	
	}

	if(c.TimeIs (3.0)){
		tantrumEnd = true;
	}

	if(c.TimeIs(44.0)){
		angryEnd = true;
	}

	if(c.TimeIs(51.0)){
		waving = true;
	}

	if(c.TimeIs(52.0)){
		GetComponent.<AudioSource>().PlayOneShot(l2);

	}

	if(c.TimeIs(68.0)){
		GetComponent.<AudioSource>().PlayOneShot(l3);
		waving = false;
	}

	if(c.TimeIs(70.0)){
		obj.SetActiveRecursively(false);
	}
	animator.SetBool("waving", waving);
	animator.SetBool("tantrum", tantrum);
	animator.SetBool("tantrumEnd", tantrumEnd);
	animator.SetBool("angryEnd", angryEnd);
 }
