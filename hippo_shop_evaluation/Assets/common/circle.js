﻿#pragma strict

internal var c = new Common();

internal static var enlargeSpeed = 0.15f;

function Start () {
	gameObject.GetComponent.<Renderer> ().enabled = false;
}

function Update () {
	if (c.GetTime() > 32.0 && c.GetTime() < 70.0) {
		gameObject.GetComponent.<Renderer> ().enabled = true;
		var x = transform.localScale.x;

		var speed = 0.0f;

		if (Input.GetKey(KeyCode.Z)) {
			speed = enlargeSpeed;
		} 
		if (Input.GetKey(KeyCode.X)) {
			speed = 0.0f;
		} 
		if (Input.GetKey(KeyCode.C) && x > 0.4) {
			speed = -enlargeSpeed;
		} 
		if (Input.GetKey(KeyCode.Q)) {
			speed = getSpeed(0.4f);
		} 
		if (Input.GetKey(KeyCode.W)) {
			speed = getSpeed(0.45f);
		} 
		if (Input.GetKey(KeyCode.E)) {
			speed = getSpeed(0.5f);
		} 
		if (Input.GetKey(KeyCode.R)) {
			speed = getSpeed(0.55f);
		} 
		if (Input.GetKey(KeyCode.T)) {
			speed = getSpeed(0.6f);
		} 
		if (Input.GetKey(KeyCode.Y)) {
			speed = getSpeed(0.65f);
		} 
		if (Input.GetKey(KeyCode.U)) {
			speed = getSpeed(0.7f);
		} 
		if (Input.GetKey(KeyCode.I)) {
			speed = getSpeed(0.75f);
		} 
		if (Input.GetKey(KeyCode.O)) {
			speed = getSpeed(0.8f);
		} 
		if (Input.GetKey(KeyCode.P)) {
			speed = getSpeed(0.85f);
		} 

		transform.localScale += c.EnlargeWithSpeed(speed);

		var currentColor : Color32;
		var maxSize = 0.9f;
		var goodSize = 0.65f;
		var minSize = 0.4f;
		var currentBlue : float;
		var currentRed : float;

		if (x < minSize) {
			currentColor = new Color32 (102, 255, 255, 255);
		} else if (x > maxSize) {
			currentColor = new Color32 (102, 255, 255, 255);
		} else if (x > goodSize) {
			currentBlue = 255 - (maxSize - x) / (goodSize - minSize) * 153;
			currentRed = 102 + (maxSize - x) / (goodSize - minSize) * 153;
			currentColor = new Color32 (currentRed, 255, currentBlue, 255);
		} else {
			currentBlue = 255 - (x - minSize) / (goodSize - minSize) * 153;
			currentRed = 102 + (x - minSize) / (goodSize - minSize) * 153;
			currentColor = new Color32 (currentRed, 255, currentBlue, 255);
		}

		gameObject.GetComponent.<Renderer> ().material.color = currentColor;
	} 

	if (c.TimeIs (70.0)) {
		gameObject.GetComponent.<Renderer> ().enabled = false;
	}
}

function getSpeed (goal : float) {
	return goal - transform.localScale.x;
}