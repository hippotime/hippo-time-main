﻿#pragma strict

import System.IO;

internal static var startAfter : double = 8;
internal static var initTime : System.DateTime = System.DateTime.Now;
internal static var user : String = "";

internal static var isPause : boolean = false;
internal static var lastTime : float = 0f;
internal static var pausedTime : float = 0f;

class Common {
	function StartLevel () {
		initTime = System.DateTime.Now;
		isPause = false;
		lastTime = 0f;
		pausedTime = 0f;
	}

	function GetTime () {
		if (isPause) {
			return lastTime; 
		} else {
			return (System.DateTime.Now - initTime).TotalSeconds - startAfter - pausedTime;
		}
	}

	function TimeIs(time : double) {
		if (isPause) {
			return false; 
		} else {
			return (GetTime () >= time) && (GetTime () <= time + Time.deltaTime);
		}
	}

	function Pause () {
		lastTime = GetTime ();
		isPause = true;
	}

	function Resume () {
		isPause = false;
		var timeDifference : float = (GetTime () - lastTime);
		pausedTime = pausedTime + timeDifference;
	}

	function SwitchPause () {
		if (isPause) {
			Resume ();
		} else {
			Pause ();
		}
	}


	function MoveToLocation (xi: float, xf: float, yi: float, yf: float, ti: float, tf: float, t: float) {
		var x = (t - ti) / (tf - ti) * (xf - xi) + xi;
		var y = (t - ti) / (tf - ti) * (yf - yi) + yi;
		return Vector3(x, y, 0);
	}

	function EnlargeWithSpeed (speed : float) {
		return Vector3(1f, 1f, 1f) * Time.deltaTime * speed;
	}


	// todo

	function GetUser () {
		return user;
	}

	function SetUser (u : String) {
		user = u;
	}

	function Log (log : String) {
		var tw = new File.CreateText(Application.persistentDataPath + "/log.txt");
		tw.WriteLine(log);
		tw.Close();
	}

}