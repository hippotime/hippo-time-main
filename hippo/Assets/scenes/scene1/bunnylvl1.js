﻿#pragma strict

var b1 : AudioClip;
var b2 : AudioClip;
var b3 : AudioClip;

internal var animator : Animator;
internal var c = new Common();
internal var isSad = false;
internal var isHappy = false;
internal var isShaking = false;

function Start () {
	animator = gameObject.GetComponent(Animator);
}

function Update () {
	if (c.TimeIs (2.0)) {
		isShaking = true;
	}

	if (c.TimeIs (3.0)) {
		GetComponent.<AudioSource>().PlayOneShot(b1);
	}

	if (c.TimeIs (10.0)) {
		isShaking = false;
	}

	if (c.TimeIs (12.0)) {
		isSad = true;
	}

	if (c.TimeIs (70.0)) {
		isSad = false;
		isHappy = true;
	}

	if (c.TimeIs (73.0)) {
		GetComponent.<AudioSource>().PlayOneShot(b2);
	}

	if (c.TimeIs (79.0)) {
		GetComponent.<AudioSource>().PlayOneShot(b3);
	}

	animator.SetBool("isSad", isSad);
	animator.SetBool("isHappy", isHappy);
	animator.SetBool("isShaking", isShaking);
}
